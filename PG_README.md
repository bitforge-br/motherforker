# PostgreSQL 10 on Fedora 28+

## Install PostgreSQL 10.x Database Server on Fedora 28+

###  Change to root user

su -
**OR**
sudo -i

### Exclude Fedora own PostgreSQL Packages

This is important step to get PostgreSQL repository working properly. Exclude PostgreSQL packages from the repository of the distro.

Add exclude to `/etc/yum.repos.d/fedora.repo` file [fedora] section:

```bash
[fedora]
...
exclude=postgresql*
```

Add exclude to /etc/yum.repos.d/fedora-updates.repo file [updates] section:

```bash
[updates]
...
exclude=postgresql*
```

### Install PostgreSQL 10.x Repository

> **ATTENTION**: See the version of Fedora you are using, we use the 28 (for example).



```
sudo dnf install https://download.postgresql.org/pub/repos/yum/10/fedora/fedora-28-x86_64/pgdg-fedora10-10-4.noarch.rpm

```

### Install PostgreSQL 10.x with DNF

```bash
sudo dnf install postgresql10 postgresql10-server postgresql10-devel postgresql10-contrib postgresql10-libs python3-flask-babelex pgadmin4-v2 
```

### Configure PostgreSQL 10.x

Initialize Cluster with initdb Command

```bash
sudo /usr/pgsql-10/bin/postgresql-10-setup initdb
```


### Set PostgreSQL Permissions

Modify PostgreSQL `/var/lib/pgsql/10/data/pg_hba.conf` (host-based authentication) file:

```bash
# Local networks
host	all	all	        xx.xx.xx.xx/xx	md5
# Example
host	all	all     	10.20.4.0/24	md5
# Example 2
host	test	testuser	127.0.0.1/32	md5
```

> **WARING**: if you whant to open it up change `md5` to `trust`

You can find more examples and full guide from PostgreSQL pg_hba.conf manual.

### Start PostgreSQL Server and Autostart PostgreSQL on Boot

Start PostgreSQL 10.x

```bash
sudo systemctl start postgresql-10.service
```

Start PostgreSQL 10.x on every boot

```bash
systemctl enable postgresql-10.service
```


## Set pg_config

find pg_config

```bash
sudo updatedb && sudo locate pg_config
```
`vim` into ~/.bash_profile and add the line before `export $PATH`:

```bash
PATH=$PATH:/usr/pgsql-10/bin/
```

Refresh terminal session

```bash
source ~/.bash_profile
```

## pgAdmin 4

Only if needed - Manually run

```bash
 /usr/pgadmin4-v2/runtime/pgAdmin4
 ```

