# Service `Cálculo Trabalhista`

Here we will show how to get a service up and running under **motherforker**


## Prerequisites

* Protobuf
* Libpqxx


## Directory Layout

```bash
|- <your_base> [your base directory for the project]
|--- local
|--- Boost.Http (use socket branch)
|--- motherforker
|--- CalculoTrabalhista
```


## Protobuf Install


```bash
sudo dnf install protobuf-devel
```

## Libpqxx


```bash
sudo dnf install libpqxx libpqxx-devel
```

## Clone the service

```bash
git clone git@gitlab.com:gianni.rossi/CalculoTrabalhista.git
```

## Database

### Create Test Database and Create New User

Change to postgres user

    sudo su - postgres
    createuser -s YOU
    exit


List Locales for DB fit

    locale -a

Create database


    CREATE DATABASE ctrab \
        WITH \
        OWNER = <YOU> \
        ENCODING = 'UTF8' \
        TABLESPACE = pg_default \
        LC_COLLATE = 'en_US.UTF-8' \
        LC_CTYPE = 'en_US.UTF-8' \
        CONNECTION LIMIT = -1;



## Dir preparation

```bash

cd http/calculo_trabalhista_service/
mkdir build
cd build
```

## CMake-GUI and MF_DIR

Run cmake ..  and cmake-gui
```bash
cmake .. 
cmake-gui ..
```

### MF_DIR

Must point for the path previously created with *CMAKE_INSTALL_PREFIX*


CMAKE_INSTALL_PREFIX = <your_path>/local

* MF_DIR = `<your_path>/local/cmake`


### CMAKE_BUILD_TYPE

Point to `Debug` under DEBUG development

    CMAKE_BUILD_TYPE = Debug


![CMAKE CMAKE_BUILD_TYPE](https://bitbucket.org/bitforge-br/motherforker/raw/master/doc_img/cmake_gui_04.png)


## Compilation

1. First: run `make` and `make install`. This should build a **`Debug`** version of the project.

```
make

make install
```

2. Second: run `cmake-gui ..` and change `CMAKE_BUILD_TYPE` from Debug to **`Relese`**

```
cmake-gui ..
```


3. Third: Re-run `make` and `make install`. This should build a __*Release*__ version of the project.

```
make

make install
```


## Running Preparation

### Copy Dirs

1. Copy all directories from `CalculoTrabalhista/http/run` into **`<your_base>/local`**

Directories: `bin`, `etc`, `log`  -> **`<your_base>/local`**

### Generate self-certificate

1. `cd` into **`<your_base>/local/etc`** and run `create_self_signed_certificate.sh`.

2. Register the password certificate with the password `1234`. If you choose to put another password, make sure to change it on `calculo_trabalhista_service.xml` file.


```
./create_self_signed_certificate.sh

```

### Run the service

1. `cd` into **`<your_base>/local`** and run `./bin/run.sh`

2. Point your borwser to `https://localhost:3000`. The server only runs under HTTP`S` connections.





