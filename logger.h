/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
/* 2017 BitForge */

#pragma once

#include "configuration.h"

#include <boost/log/trivial.hpp>
#include <boost/filesystem.hpp>

namespace mf
{

//! Class to generate all log
/*!
 * This class should be inherited by all classes that need to log events, and it needs
 * to be initialized only once - at start-up.
 *
 * To generate log, after inheriting this class, simply do:
 *
 * LOG_SEV(info) << "Information event";
 * LOG_SEV(debug) << "Debugging Information";
 *
 */
class Logger
{
public:
    typedef ::boost::log::sources::severity_logger< ::boost::log::trivial::severity_level > severity_logger;

    static void initLog(const ConfigurationPtr &_config, std::string _logId = std::string());

    severity_logger& lg() { return m_lg; }

private:
    Logger::severity_logger m_lg;
};

namespace logger
{

inline auto& get_logger(std::shared_ptr<Logger> _logger) { return _logger->lg(); }
inline auto& get_logger(Logger *_logger) { return _logger->lg(); }
inline auto& get_logger(Logger &_logger) { return _logger.lg(); }

}

/* //! Trivial severity levels
 * enum severity_level
 * {
 *     trace,
 *     debug,
 *     info,
 *     warning,
 *     error,
 *     fatal
 * };
 */

// Prefix for all logs : current file without dir, line.  eg:
// logger.h;75;
#define LOG_PREFIX \
    ::boost::filesystem::path(const_cast<char*>(__FILE__)).filename().string() << ";" << __LINE__ << ";"

// Helper to create log line with PREFIX
#define OLOG_SEV(obj, severity) \
    BOOST_LOG_SEV(::mf::logger::get_logger(obj), ::boost::log::trivial::severity_level:: severity) << LOG_PREFIX

// Helper for methods in classes that inherits from Logger class
#define LOG_SEV(severity) \
    OLOG_SEV(this, severity)

// Helper for methods where Logger class is not available
#define TLOG_SEV(severity, STR) \
    do { \
        ::mf::Logger log; \
        OLOG_SEV(log, severity) << STR ; \
    } while (false)

} // namespace mf
