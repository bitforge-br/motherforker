
set(MF_FOUND "YES")
set(MF_INCLUDE_DIR "@CMAKE_INSTALL_PREFIX@/include/")

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    set(MF_LINK_DIRECTORIES "@mf_shared_libs_path@")
else()
    set(MF_LINK_DIRECTORIES "@mf_shared_libs_path_debug@")
endif()

find_library(MF_LIBRARIES "mf_shared_libs" PATHS ${MF_LINK_DIRECTORIES})
