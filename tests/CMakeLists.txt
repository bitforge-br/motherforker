set(CMAKE_CXX_STANDARD 17)

find_package(OpenSSL)

find_package(Boost REQUIRED
                COMPONENTS
                    context
                    coroutine
                    locale
                    log
                    log_setup
                    program_options
                    regex
                    signals
                    system
                    thread
                    filesystem
            )

add_executable(sample_service_tests
    service_runner.h
    service_runner.cpp
    sample_service_tests.cpp

    ../services/samplehttpworker.h
    ../services/samplehttpworker.cpp

    ../services/samplesecurehttpworker.h
    ../services/samplesecurehttpworker.cpp

    ../services/commandworker.h
    ../services/commandworker.cpp
)

if(OpenSSL_FOUND)
    target_compile_definitions(sample_service_tests PUBLIC HAVE_OPENSSL)
endif()


target_link_libraries(sample_service_tests
    mf_shared_libs
    ${Boost_LIBRARIES}
    ${OPENSSL_LIBRARIES}
    pthread)

enable_testing()

add_test(NAME sample_service_tests
         COMMAND sample_service_tests --report_level=detailed --show_progress
         WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
