/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
/* 2017 BitForge */

#pragma once

#include <memory>

#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/spawn.hpp>

#include "logger.h"
#include "worker.h"
#include "configuration.h"

namespace mf
{

class Acceptor final : public Logger
{
public:
    typedef ::boost::asio::yield_context yield_context;
    typedef ::boost::asio::ip::tcp::acceptor acceptor;
    typedef std::vector<acceptor> AcceptorVector;

    Acceptor(::boost::asio::io_service& _ioService,
             const ServiceConfiguration *_config) noexcept;

    Acceptor(Acceptor&&) = default;

    enum Status
    {
        stStartingUp,
        stRunning,
        stStopping
    };
    Status status() const { return m_status; }

    bool isStopping() const { return m_status == stStopping; }

    void run(::boost::asio::io_service& _ioService);
    void stopListening();
    void detachWorkers();
    void stopWorkers();

    // Search WorkerVector for a worker with this pid, and restart it.
    // It might not be found, since this may belong to another acceptor
    bool restartWorkerPid(::boost::asio::io_service& _ioService,
                          pid_t _pid);

private:
    Status                                  m_status = stStartingUp;

    AcceptorVector                          m_acceptors;
    const ServiceConfiguration              *m_config;
    WorkerVector                            m_workers;

    void doAccept(yield_context _yield, acceptor *_acceptor);

    void startWorkers(::boost::asio::io_service& _ioService);
#ifndef REUSEPORT
    void startListening(::boost::asio::io_service& _ioService);
#endif
};

} // namespace mf
