# MotherForker

A complete server infrastructure for Boost.ASIO projects.

## Overview

The purpose is to implement all the basic stuff needed to get a server up-and-running,
and leaving the programmer the task of only having to worry about handling a client's request.  

The server will handle a configuration file to set-up listeners for each client. Once configured, the server will do:

1. Listen for connections on the specified port
2. Accept connection and pass them to open socket
3. each connection goes to a worker program, which is run as a seperate process from the server

The server can open multiple child processes and distribute incomming
connections between these processes to distribute the load.

Check-out the simple example client: [services/samplehttpworker.cpp](https://bitbucket.org/bitforge-br/motherforker/src/HEAD/services/samplehttpworker.cpp?at=master&fileviewer=file-view-default)

A worker process only needs to create a ConnectionBuilder, and pass it a function to handle new connections as shown below:

        ::mf::ConnectionBuilder worker(fd,
            [](::boost::asio::io_service &_ioService,
                               ::mf::ConnectionBuilder::socket_handle _socket,
                               int _port,
                               std::string _address)
            {
                auto conn = std::make_shared<SampleHttpWorker>(_ioService,
                                                            _socket,
                                                            _port,
                                                            _address);

                spawn(_ioService,
                      std::bind(&SampleHttpWorker::run, conn, std::placeholders::_1));
            }
        );

For each connection this function will be called with the socket created for the new connection.

## Architecture

At startup, the server creates a `Listener` object.  This object reads the configuration.  For each service listed, the listener creates an `Acceptor`.
Each Acceptor creates a socket for each address/port listed in the configuration, and puts that socket in the listen state.  The acceptor also
creates `Worker` objects based on the executable configured.

When a new connection arrives on the listening socket, the Acceptor passes that socket into its Worker objects, which passes the socket to the
child process to handle the connection.  So the handling and interaction with remote clients all happen on these **sub-processes**.

A Worker can either be:
1. Persistent.  A pool of processes are created at start-up, but a single process handles many client sockets.
2. On Demand.  Same as Persistent, but processes are destroyed when there is no work to be done.
3. Simlpe.  For each new connection, a new process is created, and this process will only handle one client.

## Building

This project tries to depend only on Boost libraries.  The SampleHttpWorker uses Boost.Http with is not (yet) part of the official Boost project,
but this can be ignored and removed from the CMakeLists.txt dependencies if not present. It is only there as an example.

### Prerequisite

* Fedora Development Tools & C Development Tools and Libraries
* Boost 1.60+ and boost-devel
* CMAKE 3.10+
* CMAKE-GUI
* OpenSSL-devel
* [Installed PostgreSQL 10 with development libs](./PG_README.md)

### Directory Layout

```bash
|- <your_base> [Your base directory for the project. Create it as you will. Ex.:myproject]
|--- local (used for built intermediate projects)
|--- Boost.Http (use socket branch)
|--- motherforker
```


### Fedora 27+ project compilation


1. Environment install

	sudo dnf update && sudo dnf install redhat-lsb-core

        sudo dnf groupinstall "C Development Tools and Libraries"

        sudo dnf groupinstall "Development Tools"

        sudo dnf install boost boost-devel cmake cmake-gui openssl-devel gtest gtest-devel

2. `mkdir` into your base project directory and `cd` into it

        mkdir <your base directory>
        cd <your base directory>

3. PostgreSQL Install

> [Here the Installatiton](./PG_README.md)

4. Git clone projects `boost.http` and `motherforker`

        git clone git@github.com:xvjau/boost.http.git

        cd boost.http

        git checkout socket


        cd ..


        git clone git@bitbucket.org:bitforge-br/motherforker.git

        cd motherforker

        mkdir build

        cd build

        cmake ..

        cmake-gui ..

5. CMake-GUI use default makefile 

![CMAKE Gui](https://bitbucket.org/bitforge-br/motherforker/raw/master/doc_img/cmake_gui_01.png)


6. Configure CMAKE_INSTALL_PREFIX variable on cmake-gui

> It should point to **`<your_base>/local`** path.
    
![CMAKE Prefix](https://bitbucket.org/bitforge-br/motherforker/raw/master/doc_img/cmake_gui_02.png)


7. Configure CMAKE_BUILD_TYPE (Debug)

Point to **`Debug`** under DEBUG development

        CMAKE_BUILD_TYPE = Debug

![CMAKE CMAKE_BUILD_TYPE](https://bitbucket.org/bitforge-br/motherforker/raw/master/doc_img/cmake_gui_04.png)


8. Run on cmake-gui CONFIGURE and GENERATE

![CMAKE configure generate](https://bitbucket.org/bitforge-br/motherforker/raw/master/doc_img/cmake_gui_03.png)

9. Exit `cmake-gui`

10. Run `make` and `make install`. This should generate de **`Debug`** project.

        make

        make install

11. Re-run `cmake-gui ..`
 
Point to **`Release`** under DEBUG development

        CMAKE_BUILD_TYPE = Release


```
cmake-gui ..
```

12. Run on cmake-gui CONFIGURE and GENERATE

13. Under the configured variable **`CMAKE_INSTALL_PREFIX`** path, you should get the `make install` output.

## Service compilation & installation 

We use the `Calculo_Trabalhista` service as an example.

Please, proceed [HERE](./SERVICE_README.md) for a sample service configuration.
