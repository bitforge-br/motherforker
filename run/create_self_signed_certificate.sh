#!/bin/sh

##
## Old version

#openssl genrsa -des3 -out server.key 2048
#openssl req -new -key server.key -out server.csr
#openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

#openssl x509 -outform PEM -in server.crt -out server.crt.pem
#openssl x509 -outform PEM -in server.csr -out server.csr.pem

##
## New version

openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365
