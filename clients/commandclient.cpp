#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/array.hpp>
#include <string>
#include <thread>
#include <mutex>
#include <iostream>
#include <condition_variable>

using boost::asio::ip::tcp;
using namespace std;


int commandclient(const char* host, int port, const char* command, char** response, int* responseSize)
{
    try
    {
        boost::asio::io_service io_service;

        tcp::resolver resolver(io_service);
        auto endpoint_iterator = resolver.resolve({ host, to_string(port) });
        tcp::resolver::iterator end;
        ::boost::asio::ip::tcp::socket socket(io_service);
        ::boost::system::error_code error = boost::asio::error::host_not_found;
        while (error && endpoint_iterator != end)
        {
            socket.close();
            socket.connect(*endpoint_iterator++, error);
        }
        if (error)
            throw boost::system::system_error(error);

        socket.write_some(::boost::asio::buffer(command, strlen(command) + 1));

        for (;;)
        {
            boost::array<char, 128> buf;
            boost::system::error_code error;

            size_t len = socket.read_some(boost::asio::buffer(buf), error);
            if (error == boost::asio::error::eof)
                break; // Connection closed cleanly by peer.
            else if (error)
                throw boost::system::system_error(error); // Some other error.

            std::cout.write(buf.data(), len);
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "\n" << e.what() << "\n";
    }

    return 0;
}


int main(int argc, char* argv[])
{
    if( argc == 4 )
    {
        char* response = nullptr;
        int responseSize = 0;
        char* host = argv[1];
        int port = atoi(argv[2]);
        char* command = argv[3];
        commandclient(host, port, command, &response, &responseSize);
    }
    else cout << "how to use: <host> <port> <command>\n";
}
