#include "kill_process_on_file_change.h"
#include "logger.h"

#include <errno.h>
#include <signal.h>

#include <functional>
#include <unordered_map>

#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/spawn.hpp>

namespace
{
class FileWatcher
{
public:
    FileWatcher(::boost::asio::io_service *_ioService)
    {
    }

    ~FileWatcher()
    {
    }

    bool add(::boost::filesystem::path _process, int _pid)
    {
		return false;
    }

private:
    void read(boost::asio::yield_context _yield)
    {
    }
};

} // namespace

namespace mf {

bool kill_process_on_file_change(::boost::filesystem::path _process, int _pid, ::boost::asio::io_service *_ioService)
{
    static FileWatcher watcher(_ioService);
    return watcher.add(_process, _pid);
}

} // namespace mf
