#!/usr/bin/python

import socket
import ssl

HOST = "localhost"
PORT = 5001

# create socket and connect to server
# server address is specified later in connect() method
sock = socket.socket()
sock.connect((HOST, PORT))

# wrap socket to add SSL support
sock = ssl.wrap_socket(sock,
  # flag that certificate from the other side of connection is required
  # and should be validated when wrapping
  cert_reqs=ssl.CERT_NONE
)

sock.do_handshake()

sock.send("ls /\n")
sock.send("0\n")
print sock.recv(1280)
